package Employees;

public class TesterEmp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ContractEmployee emp1= new ContractEmployee(103,"Amit",1500.0,8);
		emp1.generateSalary();
		System.out.println("Contract Employee Salary="+emp1.getSalary());
		PermanentEmployee emp2= new PermanentEmployee(211,"Akshya",60000.0,20000.0,5);
		emp2.calculateMonthlySalary();
		System.out.println("Permanent Employee Salary="+emp2.getSalary());
	}

}
