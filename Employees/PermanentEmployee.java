package Employees;

public class PermanentEmployee extends Employee{
	private double basicPay;
	private double hra;
	private float experience;
	public PermanentEmployee(int employeeId, String employeeName, double basicPay, double hra, float experience) {
		super(employeeId, employeeName);
		this.basicPay = basicPay;
		this.hra = hra;
		this.experience = experience;
	}
	public double getBasicPay() {
		return basicPay;
	}
	public void setBasicPay(double basicPay) {
		this.basicPay = basicPay;
	}
	public double getHra() {
		return hra;
	}
	public void setHra(double hra) {
		this.hra = hra;
	}
	public float getExperience() {
		return experience;
	}
	public void setExperience(float experience) {
		this.experience = experience;
	}
	
	void calculateMonthlySalary() {
		double sal;
		double vc;
		if(experience<3)
			vc=0;
		else if(experience>=3 && experience<5)
			vc=(5*this.basicPay)/100.0;
		else if(experience>=5 && experience<10)
			vc=(7*this.basicPay)/100.0;
		else
			vc=(12*this.basicPay)/100.0;
		sal=basicPay+hra+vc;
		super.setSalary(sal);
	}
}
