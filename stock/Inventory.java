package stock;

public class Inventory {
	private int quantity;
	private int lowOrderLevelQuantity;
	
	protected Inventory(int q,int l){
		quantity = q;
		lowOrderLevelQuantity = l;
	}
	public String order(int q) {
		if(q<this.quantity && q>this.lowOrderLevelQuantity) {
			return "Invoice Generated Successfully";
		}else {
			return "Request For Material Generated";
		}
	}


}
