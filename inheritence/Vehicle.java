package inheritence;

public class Vehicle {
	String color;
	int wheels;
	String model;
	public Vehicle(String color, int wheels, String model) {
		super();
		this.color = color;
		this.wheels = wheels;
		this.model = model;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getWheels() {
		return wheels;
	}
	public void setWheels(int wheels) {
		this.wheels = wheels;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void display()
	{
		System.out.println("Vehicle Details:");
		System.out.println("Vehicle color:"+color);
		System.out.println("Vehicle no of wheels:"+wheels);
		System.out.println("Vehicle model"+model);
		
	}

}
