package MyCalculator;


public class Calculator {
	
	double findAverage(double num1, double num2, double num3)
	{
		return ((num1+num2+num3)/3.0);
	}
	double findAverage(double num1, double num2, double num3,double num4)
	{
		return ((num1+num2+num3+num4)/4.0);
	}
	double findAverage(double num1, double num2, double num3,double num4, double num5)
	{
		return ((num1+num2+num3+num4+num5)/5.0);
	}
	
	public static void main(String[] args) {
		Calculator ob1 = new Calculator();
		double average1=ob1.findAverage(2,5,6);
		double average2=ob1.findAverage(2,5,17,13);
		double average3=ob1.findAverage(2,5,19.4,17,13);
		System.out.println("3 nos avg="+Math.round((average1)*100.0)/100.0);
		System.out.println("4 nos avg="+Math.round((average2)*100.0)/100.0);
		System.out.println("5 nos avg="+Math.round((average3)*100.0)/100.0);
		
	}
}

